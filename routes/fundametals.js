const express = require('express');
const router = express.Router();

require('dotenv').config()
const variableData = process.env.variableData || 'Fundamentals'

router.get('/', function (req, res, next) {
    res.send({
        name: 'Fundamentals',
        server: 'express',
        variableData: variableData
    });
});



router.get("/1", function (req, res, next) {
    function myfun(a, b) {
        return a + b;
    }
    res.send({
        name: "Sum two numbers",
        variableData: myfun(3, 4),
    });
});
router.get("/2", function (req, res, next) {
    function myfun(a, b) {
        return a === b;
    }
    res.send({
        name: "Comparison operators, strict equality",
        variableData: myfun(3, 4),
    });
});
router.get("/3", function (req, res, next) {
    function myfun(a) {
        return typeof a;
    }
    res.send({
        name: "Get type of value",
        variableData: myfun(3),
    });
});
router.get("/4", function (req, res, next) {
    function myfun(a, n) {
        return a[n - 1];
    }
    res.send({
        name: "Get nth character of string",
        variableData: myfun("3, 4", 2),
    });
});
router.get("/5", function (req, res, next) {
    function myfun(a) {
        return a.slice(3);
    }
    res.send({
        name: "Remove first n characters of string",
        variableData: myfun("huhfajslkd"),
    });
});
router.get("/6", function (req, res, next) {
    function myfun(str) {
        return str.slice(-3);
    }
    res.send({
        name: "Get last n characters of string",
        variableData: myfun("13151515"),
    });
});
router.get("/7", function (req, res, next) {
    function myfun(str) {
        return str.slice(3);
    }
    res.send({
        name: "Get first n characters of string",
        variableData: myfun("315151616"),
    });
});
router.get("/8", function (req, res, next) {
    function myfun(a) {
        return a.indexOf("is");
    }
    res.send({
        name: "Find the position of one string in another",
        variableData: myfun("praise"),
    });
});

router.get("/9", function (req, res, next) {
    function myfun(str) {
        var halfLength = Math.floor(str.length / 2);
        return str.substring(0, halfLength);
    }
    res.send({
        name: "Extract first half of string",
        variableData: myfun("545454554"),
    });
});
router.get("/10", function (req, res, next) {
    function myfun(str, n) {
        return str.slice(0, -n);
    }
    res.send({
        name: "Remove last n characters of string",
        variableData: myfun("5151515", 5),
    });
});
router.get("/11", function (req, res, next) {
    function calculatePercentage(number, percentage) {
        return (number * percentage) / 100;
    }
    res.send({
        name: "Return the percentage of a number",
        variableData: calculatePercentage(50, 100),
    });
});
router.get("/12", function (req, res, next) {
    function myfun(a, b, c, d, e, f) {
        let sum = a + b;
        let subtracted = sum - c;
        let multiplied = subtracted * d;
        let divided = multiplied / e;
        let powered = Math.pow(divided, f);
        return powered;
    }
    res.send({
        name: "Basic JavaScript math operators",
        variableData: myfun(6, 5, 4, 3, 2, 1),
    });
});
router.get("/13", function (req, res, next) {
    function concatenateStrings(a, b) {
        if (a.includes(b)) {
            return b + a;
        } else {
            return a + b;
        }
    }
    res.send({
        name: "Check whether a string contains another string and concatenate",
        variableData: concatenateStrings("cheese", "cake"),
    });
});
router.get("/14", function (req, res, next) {
    function isEven(number) {
        return number % 2 === 0;
    }
    res.send({
        name: "Check if a number is even",
        variableData: isEven(10),
    });
});
router.get("/15", function (req, res, next) {
    function countOccurrences(a, b) {
        let count = 0;
        let index = b.indexOf(a);

        while (index !== -1) {
            count++;
            index = b.indexOf(a, index + a.length);
        }

        return count;
    }
    res.send({
        name: "How many times does a character occur?",
        variableData: countOccurrences(
            "'m', 'how many times does the character occur in this sentence?'"
        ),
    });
});
router.get("/16", function (req, res, next) {
    function myfun(a) {
        return Number.isInteger(a);
    }
    res.send({
        name: "Check if a number is a whole number",
        variableData: myfun(4),
    });
});
router.get("/17", function (req, res, next) {
    function performCalculation(a, b) {
        if (a < b) {
            return a / b;
        } else {
            return a * b;
        }
    }

    res.send({
        name: "Multiplication, division, and comparison operators",
        variableData: performCalculation(10, 100),
    });
});
router.get("/18", function (req, res, next) {
    function roundToTwoDigits(a) {
        return Math.round(a * 100) / 100;
    }
    res.send({
        name: "Round a number to 2 decimal places",
        variableData: roundToTwoDigits(2.1239),
    });
});
router.get("/19", function (req, res, next) {
    function splitDigits(a) {
        let digits = Array.from(String(a), Number);
        return digits;
    }
    res.send({
        name: "Split a number into its digits",
        variableData: splitDigits(931),
    });
});




module.exports = router;