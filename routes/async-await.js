const express = require('express');
const router = express.Router();

router.get('/1', async (req, res) => {
    try {
        let promise1 = Promise.resolve(5);
        let promise2 = 44;
        let promise3 = new Promise(function(resolve, reject) {
            setTimeout(resolve, 100, 'foo');
        });

        let values = await Promise.all([promise1, promise2, promise3]);

        res.json(values);
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
});

// Creating async Function


function helloWorld() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('Hello World!');
    }, 2000);
  });
}

const msg = async function() { //Async Function Expression
  const msg = await helloWorld();
  return msg;
}

const msg1 = async () => { //Async Arrow Function
  const msg = await helloWorld();
  return msg;
}

// API endpoints
router.get('/msg', async (req, res) => {
  try {
    const message = await msg();
    res.json({ message });
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
});

router.get('/msg1', async (req, res) => {
  try {
    const message = await msg1();
    res.json({ message });
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
});



// Đặt router vào đường dẫn '/async-await'
router.use('/', router);
module.exports = router;